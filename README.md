# ndaal_YARA_passwords_weak

yara_rules includes hashed passwords of the top 200 weak passwords. The passwords are hashed in a respective rule according to the 37 following permutations:

- MD5
- sha1
- sha384
- sha224
- sha512
- sha256
- blake2b
- blake2s
- sha3_224
- sha3_256
- sha3_384
- sha3_512
- nthash
- mysql323
- mysql41
- ldap_md5
- ldap_sha1

The naming convention of the rules within the ruleset is: `<hash_permutation>_hashed_most_used_passwords`.

The rules utilise Seclist's 10-million-password-list-top-10000.txt wordlist: <https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-10000.txt>

Happy Hunting!
